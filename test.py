import AVFoundation
import Cocoa

def main():
    utterance = AVFoundation.AVSpeechUtterance.speechUtteranceWithString_("The quick brown fox jumped over the lazy dog.")

    # Configure the utterance.
    utterance.setRate_( 0.57)
    utterance.setPitchMultiplier_( 0.8)
    utterance.setPostUtteranceDelay_( 0.2)
    utterance.setVolume_( 0.8)

    # Retrieve the British English voice.
    voice = AVFoundation.AVSpeechSynthesisVoice.voiceWithLanguage_("en-GB")

    # Assign the voice to the utterance.
    utterance.setVoice_(voice)

    # Speak the utterance
    synth = AVFoundation.AVSpeechSynthesizer.alloc().init()
    synth.speakUtterance_(utterance)

    # Run the eventloop for a while
    # (this is a bodge, it would be better to use
    #  a delegate to know when the system stops
    #  speaking and stop the runloop at that point)
    Cocoa.NSRunLoop.currentRunLoop().runUntilDate_(Cocoa.NSDate.dateWithTimeIntervalSinceNow_(10))

if __name__ == "__main__":
    main()